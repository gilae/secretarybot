# SecretaryBot

A discord bot intended to remember and notify users of events, as well as save any links for later retrieval

## Disclaimer

This repository is intended for archiving and sharing purposes. It is not up-to date, nor will continue to be updated. I have removed the required auth.json and dbauth.json files, if you intend to use it!
There is a lot of messy code in this repository and for this reason, I'll be rebuilding it from the ground up, and re-uploading it to a different repository.


---

## Starting the Bot

Change to directory to which you cloned this repository and type ``node bot.js``

---

## Usage

To use the bot's commands, the form is typically ``!command [param, [param ...]]`` in Discord chat of any server where the bot is a member.


---

## Commands

``!bookEvent YYYY-MM-DD HH:mm:ss userGroup message``
Creates an event for specified time and userGroup

``!reminderOn``
Turns on automated reminders for upcoming events in a certain channel

``!upcoming``
Manually checks for upcoming events

``removeEvent ID [, ID[, ...]]``
Removes events with the specified IDs (retrieved from !agenda, or !upcoming)

``!agenda [TODAY|TOMORROW|Date]``
Lists events for the specified parameter

``React to a link with :ok_hand: react``
Save a link someone posted for later retrieval with !links

``!links [YYYY-MM-DD] [User [, User[, ...]]]``
Show links from users and/or dates specified

