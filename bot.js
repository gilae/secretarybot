var Discord = require('discord.js');
var logger = require('winston');
var auth = require('./auth.json');
var dbfunc = require('./db.js');
var mysql = require('mysql');
var moment = require('moment-timezone');

const pattern = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gi;
var okhand = '%F0%9F%91%8C';
var thinking = '%F0%9F%A4%94';
var interval = 6000;
var remindTime = "30 minute";
//var myTimezone = "America/Toronto";
var myDatetimeFormat = "YYYY-MM-DD HH:mm:ss";
var myDateFormat = "YYYY-MM-DD";
var d;
var tmpsql;
var regex1 = new RegExp(pattern);
//var reminder;
//var tempReminder = undefined;
// Configure logger settings

const embed = {
  "title": "How to read:",
  "description": "Values in [square brackets] are optional\n__Underlined__ values are required\n... means 0 or more times",
  "color": 14948781,
  "thumbnail": {
    "url": "http://images.clipartpanda.com/orange-question-mark-clipart-question-mark-hi.png"
  },
  "fields": [
    {
      "name": "!bookEvent __YYYY-MM-DD HH:mm:ss__ __user-group__ __message__",
      "value": "Create an event for specified time and user-group"
    },
    {
      "name": "!reminderOn",
      "value": "Turn on automated reminders for upcoming events"
    },
    {
      "name": "!upcoming",
      "value": "Manually checks for upcoming events"
    },
    {
      "name": "!removeEvent __ID__ [ID ...]",
      "value": "Removes events with specified IDs fromreminder table"
    },
    {
      "name": "!agenda [Today|Tomorrow|Date]",
      "value": "Display events for today/tomorrow/date"
    },
    {
      "name": "React to a link with :ok_hand:",
      "value": "Save a link someone posted fo later retrieval with !links"
    },
    {
      "name": "!links [YYYY-MM-DD] [User User ...]",
      "value": "Show links for users and dates specified"
    }
  ]
};

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
	colorize: true
});
logger.level = 'debug';

var bot = new Discord.Client();

bot.on('ready',function() {
	logger.info('Connected');
	logger.info('Logged in as: ');
	logger.info(bot.user.username + ' - (' + bot.user.id + ')');
});

function repeatingCheck(serverID,cID){
	setTimeout(function(){
		checkSchedule(serverID,setScheduleQuiet,cID);
		isEventNow(serverID,setScheduleNow,cID);
		repeatingCheck(serverID,cID);
	},interval);
}

function isLeapYear(pYear){
	if((pYear % 4) != 0) return 0;
	if((pYear % 100) == 0) return 1;
	if((pYear % 400) == 0) return 1;
	return 0;
}

function daysInMonth(pMonth,leapYear){
	var days;
	if (pMonth == 2){
		days = 28+leapYear;
	}
	else {
		days = 31 - (((pMonth - 1) % 7) % 2);
	}
	return days;
}

function checkIfDate(pDate){
	if (pDate.indexOf("-") != 4) return -1;
	var parts = pDate.split("-");
	var i;
	if (parts.length != 3) return -1;
	for (i = 0; i < 3; i++){
		if (isNaN(parts[i])) return -1;
	}
	var numParts = [parseInt(parts[0],10),parseInt(parts[1],10),parseInt(parts[2],10)];
	if (numParts[0] < 1000 || numParts[0] > 3000) return -1;
	if (numParts[1] < 1 || numParts[1] > 12) return -1;
	if (numParts[2] < 1 || numParts[2] > daysInMonth(numParts[1],isLeapYear(numParts[0]))) return -1;
	return 1;

}

function setSchedule(value,cID){
	if(value.length > 0){
		for (var i = 0; i < value.length; i++){
		var reminder = "The next event, " + value[i].remindertext + ' is at: ' + value[i].remindertime + ' for ' + value[i].GroupCall;
	//reminder = value;
		cID.send(reminder);
		}
	}
//	else{
//		bot.sendMessage({
//			to:cID,
//			message: "No events within " + remindTime + "(s)"
//		});
//	}
}

function setScheduleQuiet(value,cID){
	if(value.length > 0) {
		for (var i = 0; i < value.length; i++){
			var reminder = value[i].GroupCall + ', ' + value[i].remindertext + 'at: ' + value[i].remindertime;
			cID.send(reminder)
		}
	}
//	else{
//		logger.info("No Event");
//	}
}

function setScheduleNow(value,cID){
	if(value.length > 0){
		for (var i = 0; i < value.length; i++){
			var reminder = value[0].GroupCall + '! ' + value[0].remindertext + 'HAPPENING NOW!';
			cID.send(reminder);
		}
	}
}

function postAllEvents(value,cID){
	var i;
	if(value.length > 0){
		for(i = 0; i < value.length; i++){
			cID.send("(" + value[i].reminderID + ") " + value[i].remindertime + " -- " + value[i].remindertext);
		}
	}
	else{
		cID.send("No Events");
	}
}

function sendAllResults(value,cID){
	var i;
	if(value.length > 0){
		for(i = 0; i < value.length; i++){
			cID.send("(" + value[i].username + ") -- " + value[i].linkdef);
		}
	}
	else{
		cID.send("No Links");
	}
}

function doNothing(value,cID){
	return;
}

function verify(value,cID){
	cID.send("Done");
}

function messageToChannel(mText,cID){
		cID.send(mText);
}

function checkSchedule(serverID,callbackF,channID){
//	dbfunc.doConnect();
	//var query1 = ("SELECT * FROM SECRETARY.reminders t1 LEFT JOIN SECRETARY.usergroups t2 ON t1.GroupCode = t2.GroupCode WHERE t1.ReminderStatusID=3 AND t1.ReminderTime BETWEEN NOW() AND ADDDATE(NOW(), INTERVAL " + remindTime + " MINUTE) ORDER BY t1.ReminderTime ASC LIMIT 1;");
	var query = ["CREATE TEMPORARY TABLE SECRETARY.tmpEvents SELECT t1.*,t2.GroupCall,t3.serverdef FROM SECRETARY.reminders t1 INNER JOIN SECRETARY.servers t3 ON t3.serverID=t1.serverID LEFT JOIN SECRETARY.usergroups t2 ON t1.usergroupID = t2.usergroupID WHERE t1.ReminderStatusID = 3 AND t3.serverdef = ? AND t1.ReminderTime BETWEEN NOW() AND ADDDATE(NOW(), INTERVAL " + remindTime + " ) ORDER BY t1.ReminderTime ASC LIMIT 1;","SELECT * FROM SECRETARY.tmpEvents;","UPDATE SECRETARY.reminders SET ReminderStatusID = 4 WHERE ReminderID IN (SELECT ReminderID FROM SECRETARY.tmpEvents);","DROP TABLE IF EXISTS SECRETARY.tmpEvents;"];
	var inserts = [serverID];
	query[0]=mysql.format(query[0],inserts);
	dbfunc.doQuery(query,callbackF,channID);
//	dbfunc.endConnect();
//	reminder = "The next event, " + result[0].ReminderText + ' is at: ' + result[0].ReminderTime + ' for ' + result[0].GroupCall;
}

function isEventNow(serverID,callbackF,channID){
	var query = ["CREATE TEMPORARY TABLE SECRETARY.tmpEventsNow SELECT t1.*,t2.GroupCall,t3.serverdef FROM SECRETARY.reminders t1 LEFT JOIN SECRETARY.usergroups t2 ON t1.usergroupID = t2.usergroupID INNER JOIN SECRETARY.servers t3 ON t3.serverID=t1.serverID WHERE t1.ReminderStatusID != 5 AND t3.serverdef = ? AND t1.ReminderTime BETWEEN NOW() AND ADDDATE(NOW(), INTERVAL 5 MINUTE) ORDER BY t1.ReminderTime ASC LIMIT 1;","SELECT * FROM SECRETARY.tmpEventsNow;","UPDATE SECRETARY.reminders SET ReminderStatusID = 5 WHERE ReminderID IN (SELECT ReminderID FROM SECRETARY.tmpEventsNow);","DROP TABLE IF EXISTS SECRETARY.tmpEventsNow;"];
	var inserts = [serverID];
	query[0] = mysql.format(query[0],inserts)
	dbfunc.doQuery(query,callbackF,channID);
}

function getLinks(args,serverID,callbackF,channID){
	//Potential args: day, user, good meme or bad meme
	var isAll = 0;
	var day;
	var names = [];
	var qTmp = ["SELECT t1.linkdef,t2.username FROM SECRETARY.links t1 INNER JOIN SECRETARY.users t2 ON t1.userID=t2.userID INNER JOIN SECRETARY.servers t3 ON t3.serverID=t1.serverID WHERE t3.serverdef = ?"];
	var inserts = [serverID];
	qTmp[0] = mysql.format(qTmp[0],inserts);
	args.map(s => {	
		if(s === "-a"){
		isAll = 1;
		}
		else if(checkIfDate(s) == 1){
		day = s;
		}
		else
		{
		names.push(s);
		}
	});
	if (isAll == 0) {
		qTmp[0] = qTmp[0] + " AND t1.messagestatusID = 3";
	}
	if (day != undefined){
		qTmp[0] = qTmp[0] + " AND t1.createdate BETWEEN ? AND ADDDATE( ? , INTERVAL 1 DAY)";
		inserts = [day, day];
		qTmp[0] = mysql.format(qTmp[0],inserts);
	}
	if (names.length > 0){
		qTmp[0] = qTmp[0] + " AND t2.username IN ?"
		inserts = [names];
		qTmp[0] = mysql.format(qTmp[0],[inserts]);
	}
	qTmp[0] = qTmp[0] + " LIMIT 50;";
	dbfunc.doQuery(qTmp,callbackF,channID);
	//logger.info(qTmp[0]);
	
}


function isEventToday(day,serverID,callbackF,channID){
        var query = ["SELECT t1.*,t2.GroupCall FROM SECRETARY.reminders t1 LEFT JOIN SECRETARY.usergroups t2 ON t1.usergroupID = t2.usergroupID INNER JOIN SECRETARY.servers t3 ON t3.serverID=t1.serverID WHERE t1.ReminderStatusID != 5 AND t3.serverdef = ? AND t1.ReminderTime BETWEEN ? AND ADDDATE( ? , INTERVAL 1 DAY) ORDER BY t1.ReminderTime;"];
        var inserts = [serverID,day, day];
	query[0] = mysql.format(query[0],inserts);
//	logger.info(query[0]);
	dbfunc.doQuery(query,callbackF,channID);
}

function reminderCleanup(callbackF,channID){
	var query = ["UPDATE SECRETARY.reminders SET reminderstatusID = 5 WHERE reminderstatusID != 5 AND remindertime < NOW()"];
	dbfunc.doQuery(query,callbackF,channID);
}

function disableReminder(serverID,reminderID,callbackF,channID){
	var query =["UPDATE t1 SET t1.reminderstatusID = 5 FROM SECRETARY.reminders t1 INNER JOIN SECRETARY.servers t3 ON t3.serverID=t1.serverID WHERE t1.reminderID = ? AND t3.serverdef = ?;"]
	var inserts = [reminderID,serverID];
	query[0] = mysql.format(query[0],inserts);
	dbfunc.doQuery(query,callbackF,channID);
}

function addServer(serverdef,callbackF,channID){
	var query = ["INSERT IGNORE INTO SECRETARY.servers(serverdef) VALUES (?)"];
	var inserts = [serverdef];
	query[0] = mysql.format(query[0],inserts);
	dbfunc.doQuery(query,callbackF,channID);
}

function addUser(user,callbackF,chann){
	var query = ["INSERT IGNORE INTO SECRETARY.users(username,usercode) VALUES (?,?)"];
	var inserts = [user.username,user.id];
	query[0] = mysql.format(query[0],inserts);
	dbfunc.doQuery(query,callbackF,chann);
}

bot.on('message', function (message) {

var user = message.author;
var userID = message.author.id;
var mChannel = message.channel;
var messageID = message.id;
var messageContent = message.content;
var serverID = mChannel.guild.id;

addUser(user,doNothing,mChannel);
//logger.info(messageID);

if (messageContent.substring(0,1) == '!') {
var args = messageContent.substring(1).split(' ');
var cmd = args[0];
args = args.splice(1);

switch(cmd) {
	case 'ping':
	mChannel.send('pong');
	break
	case 'dbtest':
	var connection =dbfunc.connect;
	if(connection == 1){
		mChannel.send('connecting to db')
	}
	else{
		mChannel.send('connections failed, code:' + connection)
	}
	break;
	case 'upcoming':	
	checkSchedule(serverID,setSchedule,mChannel);
	//bot.sendMessage({
	//	to: channelID,
	//	message: reminder[0].ReminderText
	//});
	break;
	case 'reminderOn':
	repeatingCheck(serverID,mChannel);
	break;
	case 'bookEvent':
	if (args.length >= 4 && checkIfDate(args[0]) == 1){ 
	var reminderTime = args[0] + " " + args[1];
	d = new Date(reminderTime);
	reminderTime = moment(d).format(myDatetimeFormat);
	var groupCode = args[2];
	var reminderText = "";
	for (var i = 3; i < args.length; i++){
		reminderText = reminderText + args[i] + ' ';
	}
	var inserts = [reminderTime,reminderText,serverID,groupCode];
	var queryI = ["INSERT INTO SECRETARY.reminders(ReminderTime, ReminderText,serverID, usergroupID, ReminderStatusId) SELECT ? , ? ,(SELECT serverID FROM SECRETARY.servers WHERE serverdef = ? LIMIT 1) AS serverID,ug.usergroupID ,3 FROM SECRETARY.usergroups ug WHERE groupCode = ? ;"];
	queryI[0] = mysql.format(queryI[0],inserts)
	dbfunc.doQuery(queryI,verify,mChannel);
	}
	else {
	mChannel.send("Correct usage is: !bookEvent {yyyy-mm-dd HH:mm:ss} {group} {message}");
	}
	break;
	case 'setInterval':
	if (args.length == 1){
	interval = args[0]*1000;
	mChannel.send("Schedule-check interval set to: " + args[0] + " seconds.");
	}
	break;
	case 'usage':
	mChannel.send("**USAGE**", { embed });
	break;
	case 'agenda':
	var inp = "";
	for (var k = 0; k < args.length; k++){
		inp = inp + args[k] + " ";
	}
	//logger.info(inp);
	//inp = inp.trim;
	d = new Date(inp);
	//logger.info(d);
	if (args.length == 0 || args[0].toLowerCase() == "today"){
		d = new Date();
		tmpsql = moment(d).format(myDateFormat);
		isEventToday(tmpsql,serverID,postAllEvents,mChannel);	
	}
	else if (args[0].toLowerCase() == "tomorrow"){
		d = new Date();
		d.setDate(d.getDate()+1);
		tmpsql = moment(d).format(myDateFormat);
		isEventToday(tmpsql,serverID,postAllEvents,mChannel);
	}
	else if (moment(d).format(myDateFormat) != "Invalid Date") {
		//d = new Date(inp);
		tmpsql = moment(d).format(myDateFormat);
		isEventToday(tmpsql,serverID,postAllEvents,mChannel);
	//	logger.info("'"+args[0]+"'");
	}
	else {
		mChannel.send("Correct usage is -- !agenda [today] | !agenda {tomorrow|date}");
	}
	break;
	case 'devCleanup':
	reminderCleanup(doNothing,mChannel);	
	break;
	case 'removeEvent':
	if(args.length < 1){
		mChannel.send("Correct usage is -- !removeEvent {ID} [ ID ID ID ...] || ID obtainable through !agenda")
	}
	else{
		var check = 0;
		var j;
		for (j = 0; j < args.length; j++){
			if(!isNaN(parseInt(args[j],10))){
				disableReminder(serverID,args[j],doNothing,mChannel);
				check++;
			}
		}
		if (check < 0){
				mChannel.send("Please provide at least 1 valid ID");
		}
		else {
				mChannel.send("Up to " + check + " events disabled");
		}
	}
	break;
	case 'addServer':
	addServer(serverID,verify,mChannel);	
	break;
	case 'links':
	getLinks(args,serverID,sendAllResults,mChannel);	
	break;	
	}
}
	var toMatch = messageContent;
	var matched;
	var mList = [];
	while((matched = regex1.exec(toMatch)) !== null){
		mList.push(matched[0]);
	}
	if(mList.length>0 && user != bot.user){
		var queryLinks = ["CREATE TEMPORARY TABLE SECRETARY.tmpLinks(linkdef varchar(500));","INSERT INTO SECRETARY.tmpLinks(linkdef) VALUES (?);","REPLACE INTO SECRETARY.links(messageID,linkdef,serverID,userID,messagestatusID,createdate) SELECT ?,t1.linkdef,(select serverID FROM SECRETARY.servers WHERE serverdef = ?),(SELECT userID FROM SECRETARY.users where username = ?),2,NOW() FROM SECRETARY.tmpLinks t1","DROP TABLE IF EXISTS SECRETARY.tmpLinks;"];
		var inserts= [[mList],[messageID,serverID,user.username]];
		queryLinks[1] = mysql.format(queryLinks[1],inserts[0]);
		queryLinks[2] = mysql.format(queryLinks[2],inserts[1]);
		//logger.info(queryLinks);
		dbfunc.doQuery(queryLinks,doNothing,mChannel);
	}

});

bot.on('messageReactionAdd',(messageReaction,user) => {
	//if (messageReaction.emoji.name === )
	var sql = [];
	var inserts = [];
	if (messageReaction.emoji.identifier === okhand){
		sql = ["UPDATE SECRETARY.links SET messagestatusID = 3 WHERE messageID = ?;"];
		inserts = [messageReaction.message.id];
		sql[0] = mysql.format(sql[0],inserts);
		dbfunc.doQuery(sql,doNothing,messageReaction.message.channel);
	} else if (messageReaction.emoji.identifier === thinking)
	{
		sql = ["UPDATE SECRETARY.links SET messagestatusID = 4 WHERE messageID = ?;"];
		inserts = [messageReaction.message.id];
		sql[0] = mysql.format(sql[0],inserts);
		dbfunc.doQuery(sql,doNothing,messageReaction.message.channel);
	}
});

bot.on('messageReactionRemove',(messageReaction,user) => {
	var sql = [];
	var inserts = [];
	if (mesageReaction.emoji.identifier === okhand || messageReaction.emoji.identifier === thinking){
		sql = ["UPDATE SECRETARY.links SET messagestatusID = 2 WHERE messageID = ?;"];
		inserts = [messageReaction.message.id];
		sql[0] = mysql.format(sql[0],inserts);
		dbfunc.doQuery(sql,doNothing,messageReaction.message.channel);
	}
});

bot.login(auth.token);
//var scheduleCheck = new Discord.generateMessage({
//	channel: '434523667523829774',
//	server: '434523667523829772',
//	content: '!
//});

//setInterval(function(){
//	bot.emit(bot,"!upcoming");
//	logger.info("emitted");
//},interval);
